import { ITour } from "src/interface/tour"

export class TourDto implements ITour {
    name: string;
    location: string;
    description: string;
    date: string;
    tourOperator: string;
    price: number;
    img: string;
    id: string;
    type: string;

    constructor(name, location, description, date, tourOperator, price, img, type) {
        this.name = name;
        this.location = location;
        this.description = description;
        this.date = date;
        this.tourOperator = tourOperator;
        this.price = price;
        this.img = img;
        this.type = type;
    }

}